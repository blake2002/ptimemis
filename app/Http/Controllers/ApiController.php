<?php

namespace App\Http\Controllers;

use DB;
use Storage;
use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Request;

class ApiController extends BaseController
{
	/**
     * Show the config.
     */
    public function config($table)
    {
		$result = $this::tableConfig($table);
		return $this::jsonResponse(false,$result);
	}

	/**
     * List the index.
     */
    public function index(Request $request,$table)
    {
    	$data          = $request->all();

        //分页
    	$pageNum       = isset($data["pageNum"])?$data["pageNum"]:1;
    	$countPerPage  = isset($data["countPerPage"])?$data["countPerPage"]:50;
    	$numStart      = $countPerPage*($pageNum-1);

        $configResult = $this::tableConfig($table);
        

        $filedStr  = ''; //字段过滤
        $relateStr = ''; //表关联
        $filterStr  = ''; //内容过滤

        //字段过滤 表关联
        foreach ($configResult['fields'] as $field) {
            $fieldName = $field['name'];

            if(!$field['is_hide']){
                $filedStr   .= "$table.$fieldName,";
            }
            // var_dump($fieldStr);exit;
            $sameTableNum = 1;
            if(isset($field['relate']) && !isset($data['no_relate'])){
                $relateTable    = $field['relate']['table'];
                $relateField    = $field['relate']['field'];
                $relateSelect   = $field['relate']['select'];
                
                $relateTableOrigin = $relateTable;
                if($relateTable == $table){
                    $relateTable = $table.$sameTableNum++;
                }

                $filedStr   .= "$relateTable.$relateSelect AS ".$relateTableOrigin."_$relateSelect,";
                $relateStr  .= "LEFT JOIN $relateTableOrigin AS $relateTable ON $relateTable.$relateField = $table.$fieldName ";
            }
        }            
        
        //内容过滤
        if(isset($data['filter'])){
            foreach ($data['filter'] as $filter) {
                $filterField  = $filter[0];
                $filterSignal = $filter[1];
                $filterValue  = $filter[2];
                $filterStr .= "$table.$filterField $filterSignal '$filterValue' AND ";
            }
        }
        if(isset($data['filter_str'])){
            $filterStr .= $data['filter_str']." AND ";
        }

        $filedStr  = substr($filedStr, 0, -1);
        $filterStr = $filterStr==''?'1':substr($filterStr, 0, -5);
        
        //执行
        $result['pageNum'] = $pageNum;
        $result['countPerPage'] = $countPerPage;
        $count = DB::table($table)->count();
        $result['lastPage'] = ceil($count/$countPerPage);
		$result['data'] = DB::select("
            SELECT $filedStr FROM $table
            $relateStr
            WHERE $filterStr
            LIMIT $numStart,$countPerPage
        ");
		return $this::jsonResponse(false,$result);
	}

	/**
     * Show one object.
     */
    public function show($table,$id)
    {
		$result = DB::select("SELECT * FROM $table WHERE id=$id");
		return $this::jsonResponse(false,isset($result)?$result[0]:[]);
	}

	/**
     * Store one object.
     */
    public function store(Request $request,$table)
    {
    	$data = $request->all();
        if(!$data){
            return $this::jsonResponse(true,'缺少重要参数');
        }
		//$result = DB::insert('insert into $table (id, name) values (?, ?)', array(1, 'Dayle'));
		$result = DB::table($table)->insertGetId($data);
		return $this::jsonResponse(false,$result);
	}

	/**
     * Update one object.
     */
    public function update(Request $request,$table,$id)
    {
    	$data = $request->all();
		//$result = DB::update('update $table set votes = 100 where id = $id';
		$result = DB::table($table)->where("id",$id)->update($data);
		return $this::jsonResponse(false,$result);
	}

	/**
     * Destroy one object.
     */
    public function destroy($table,$id)
    {
		$result = DB::delete("DELETE FROM $table WHERE id=$id");
		return $this::jsonResponse(false,$result);
	}
}
