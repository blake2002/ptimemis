<?php

namespace App\Http\Controllers;

use DB;
use Storage;
use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Response;

class ConfigController extends BaseController
{
	public function init()
	{

		$tables = DB::select("SHOW TABLE STATUS;");
		foreach ($tables as $table) {
			$tableConfig = [];
			$tableConfig["base"] = [
				"name"		=> $table->Name,
				"comment"	=> $table->Comment
			];

			$tableName = $table->Name;
			$tableDetail = DB::select("SHOW FULL COLUMNS FROM $tableName");

			foreach ($tableDetail as $item) {
				$tableConfig["fields"][] = [
					"name"		=> $item->Field,
					"type"		=> $item->Type,
					"null"		=> $item->Null,
					"key"		=> $item->Key,
					"default"	=> $item->Default,
					"comment"	=> $item->Comment,
					"is_hide"	=> 0,
				];
			}

			$filePath = "config/$tableName.php";
			if (Storage::exists($filePath)) {
			    Storage::move($filePath, "config_back/$tableName".date("YmdHis").".php");
			}
			Storage::put("config/$tableName.php", "<?php return ".var_export($tableConfig,true).";");
		}

	}
	
}