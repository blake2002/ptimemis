<?php return array (
  'base' => 
  array (
    'name' => 'album',
    'comment' => '相册表',
  ),
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => 'PRI',
      'default' => NULL,
      'comment' => '',
      'is_hide' => 0,
    ),
    1 => 
    array (
      'name' => 'category_id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '所属分类id',
      'is_hide' => 0,
      'relate'=> [
        'table' =>'category',
        'field' =>'id',
        'select'=>'name',
        'filter' =>"category.module = 'album'",
        'name'  =>'category_name',
        'comment' => '所属分类'
      ]
    ),
    2 => 
    array (
      'name' => 'title',
      'type' => 'varchar(40)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '标题',
      'is_hide' => 0,
    ),
    3 => 
    array (
      'name' => 'img_dir',
      'type' => 'varchar(200)',
      'null' => 'YES',
      'key' => '',
      'default' => NULL,
      'comment' => '图片地址',
      'is_hide' => 0,
      'is_img'  =>1,
    ),
    4 => 
    array (
      'name' => 'description',
      'type' => 'varchar(200)',
      'null' => 'YES',
      'key' => '',
      'default' => NULL,
      'comment' => '描述',
      'is_hide' => 1,
    ),
    5 => 
    array (
      'name' => 'created_at',
      'type' => 'datetime',
      'null' => 'YES',
      'key' => '',
      'default' => 'CURRENT_TIMESTAMP',
      'comment' => '创建时间',
      'is_hide' => 0,
    ),
    6 => 
    array (
      'name' => 'created_by',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '创建用户id',
      'is_hide' => 0,
      'relate'=> [
        'table' =>'user',
        'field' =>'id',
        'select'=>'name',
        'where' =>'',
        'name'  =>'user_name',
        'comment' => '创建用户'
      ]
    ),
    7 => 
    array (
      'name' => 'is_visible',
      'type' => 'tinyint(1)',
      'null' => 'YES',
      'key' => '',
      'default' => '1',
      'comment' => '是否显示',
      'is_hide' => 0,
    ),
    8 => 
    array (
      'name' => 'is_delete',
      'type' => 'tinyint(1)',
      'null' => 'YES',
      'key' => '',
      'default' => '0',
      'comment' => '是否已删除',
      'is_hide' => 0,
    ),
    9 => 
    array (
      'name' => 'weight',
      'type' => 'smallint(6)',
      'null' => 'YES',
      'key' => '',
      'default' => '0',
      'comment' => '排序权重',
      'is_hide' => 0,
    ),
  ),
);