(function(){
	var menuData = [
		{
			'name'			: '数据管理',
			'description'	: '对系统内容数据进行管理',
			'ico'			: 'pencil',
			'url'			: '#',
			'son'			:
			[
				{
					'name'			: '分类管理',
					'description'	: '对系统分类进行管理',
					'ico'			: 'pencil',
					'state'			: ['object',{'objectName':'category'}]				
				},
				{
					'name'			: '文章管理',
					'description'	: '对文章进行增删改查等管理',
					'ico'			: 'pencil',
					'state'			: ['object',{'objectName':'article'}]			
				},
				{
					'name'			: '链接管理',
					'description'	: '',
					'ico'			: 'pencil',
					'state'			: ['object',{'objectName':'link'}]				
				},
				{
					'name'			: '相册管理',
					'description'	: '对相册进行增删改查等管理',
					'ico'			: 'pencil',
					'state'			: ['object',{'objectName':'album'}],
					'son'			:	
					[
						{
							'name'			: '相册管理',
							'description'	: '对相册进行增删改查等管理',
							'ico'			: 'pencil',
							'state'			: ['object',{'objectName':'album'}]	
						},
						{
							'name'			: '图片管理',
							'description'	: '对相册中的图片进行增删改查等管理',
							'ico'			: 'pencil',
							'state'			: ['object',{'objectName':'picture'}]	
						}
					]
				},
				{
					'name'			: '专题管理',
					'description'	: '',
					'ico'			: 'pencil',
					'state'			: ['object',{'objectName':'topic'}],
					'son'			:	
					[
						{
							'name'			: '专题管理',
							'description'	: '对专题进行增删改查等管理',
							'ico'			: 'pencil',
							'state'			: ['object',{'objectName':'topic'}]	
						},
						{
							'name'			: '专题分类关联管理',
							'description'	: '对专题中关联的分类进行增删改查等管理',
							'ico'			: 'pencil',
							'state'			: ['object',{'objectName':'topic_category'}]	
						}
					]		
				},
				{
					'name'			: '标签管理',
					'description'	: '',
					'ico'			: 'pencil',
					'state'			: ['object',{'objectName':'tag'}]	
				},
				{
					'name'			: '评论管理',
					'description'	: '对评论进行增删改查等管理',
					'ico'			: 'pencil',
					'state'			: ['object',{'objectName':'comment'}]			
				},
			]
		},
		{
			'name'			: '系统管理',
			'description'	: '',
			'ico'			: 'pencil',
			'url'			: '#',
			'son'			:
			[
				{
					'name'			: '用户管理',
					'description'	: '',
					'ico'			: 'pencil',
					'state'			: ['object',{'objectName':'user'}],
					'son'			:
					[
						{
							'name'			: '用户管理',
							'description'	: '',
							'ico'			: 'pencil',
							'state'			: ['object',{'objectName':'user'}]					
						},
						{
							'name'			: '权限管理',
							'description'	: '',
							'ico'			: 'pencil',
							'state'			: ['object',{'objectName':'permission'}]			
						},
						{
							'name'			: '角色管理',
							'description'	: '',
							'ico'			: 'pencil',
							'state'			: ['object',{'objectName':'role'}]			
						}
					]
				},
				{
					'name'			: '系统配置',
					'description'	: '',
					'ico'			: 'pencil',
					'state'			: ['object',{'objectName':'setting'}]		
				},
			]
		}
	];


	var myApp = angular.module('myApp', ['lumx','ui.router','ngResource','object']);
	//http://ui.lumapps.com/
	//https://docs.angularjs.org/api/ngResource/service/$resource
	//http://angular-ui.github.io/ui-router/site/#/api

	myApp.config(function($stateProvider, $urlRouterProvider) {

	    // For any unmatched url, redirect to /
	    $urlRouterProvider.otherwise("/");
	   
	    // Now set up the states
		$stateProvider
			.state('defaul', {
	    		url: "/",
	    		templateUrl: "views/main/index.html"
	  		})
	  		.state('object', {
	    		url: "/object/:objectName",
	    		templateUrl: "views/object/object_list.html",
	    		controller:'ObjectController'
	  		});

  	});

  	myApp.factory('API', ['$resource', function($resource) {
		return $resource('/:object/:id', null,
    	{	
        	'update': 	{ method:'PUT' 	},
        	'query':  	{ method:'GET'  }
    	});
	}]);

	myApp.controller('GlobalController', [
        '$scope', '$http', '$state', '$rootScope', function($scope,$http,$state,$rootScope){
        	

        	$scope.stateGo = function(menuTwo){

        		$state.go(menuTwo.state[0],menuTwo.state[1]);
        		return false;
        	}

        	$rootScope.$on('$stateChangeSuccess',
			function(event, toState, toParams, fromState, fromParams){

				angular.forEach(menuData, function(menuOne) {
					if(menuOne.son){
						angular.forEach(menuOne.son, function(menuTwo) {
							if(menuTwo.son){
								angular.forEach(menuTwo.son, function(menuThree) {
									if($state.is(menuThree.state[0],menuThree.state[1])){
							  			$scope.breadcrumbData = [menuOne,menuTwo,menuThree];
							  		}
								});
							}else{
						  		if($state.is(menuTwo.state[0],menuTwo.state[1])){
						  			$scope.breadcrumbData = [menuOne,menuTwo];
						  		}								
							}

					  	});
					}else{
						if($state.is(menuOne.state[0],menuOne.state[1])){
				  			$scope.breadcrumbData = [menuOne];
				  		}
					}
				});

				$scope.menuData = menuData;
			});
        }
    ]);

})();
